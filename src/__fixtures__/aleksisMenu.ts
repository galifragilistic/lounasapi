export const monday = {
	url: '',
	name: '',
	menuItems: [
		'Lounas –',
		'Fajitas pannupihvejä, cheddarkastiketta, papuja, riisiä ja perunaa l g vs Ikan molee maleasialaista kalacurrya ja riisiä m , g vs Artisokansydän-kirsikkatomaatti-pasta m',
		'Grill –',
		'Nasi goreng; paistettua riisiä, katkarapuja ja possua m vs,p',
		'Deli –',
		'Salaattivalikoima & kurpitsasosekeittoa l g Lisäkkeenä: broileria, emmentalia, raejuustoa, härkistä'
	],
	details: {
		address: 'Kalevantie 2, 33100 Tampere',
		homepage: 'www.ravintola-aleksis.fi',
		stars: '3.3 (3 arviota)',
		openingHours: 'ma-pe: 10:30-13:30',
		restaurantType: '',
		kitchenType: ''
	}
}
export const thursday = {
	url: '',
	name: '',
	menuItems: [
		'Lounas –',
		'Porsaan ulkofileetä piparjuuri-smetanakastikkeella ja perunoita l g Kaalilaatikkoa ja puolukkahilloa m , g Vuohenjuustolla gratinoituja punajuuriröstejä ja riisiä vl g',
		'Grill –',
		'Broiler-mozzarellabagel ja salaattia l',
		'Deli –',
		'Salaattivalikoima & meksikolaista papu-kasviskeittoa veg , g vs Lisäkkeenä: broileria, cheddaria, tofua'
	],
	details: {
		address: 'Kalevantie 2, 33100 Tampere',
		homepage: 'www.ravintola-aleksis.fi',
		stars: '3.3 (3 arviota)',
		openingHours: 'ma-pe: 10:30-13:30',
		restaurantType: '',
		kitchenType: ''
	}
}
export const friday = {
	url: '',
	name: '',
	menuItems: [
		'Lounas –',
		'Pekoni-taatelibroileria, aiolia, paholaisenhilloa, riisiä ja vehnää m Tomaatti-brovoloneravioleja tomaattikastikkeella Lohimurekepihvejä, kananmunakastiketta ja perunoita l',
		'Grill –',
		'Pepperjack-juustoburger ja lohkoperunoita Pyydettäessä kasvispihvi ja gluteeniton sämpylä',
		'Deli –',
		'Salaattivalikoima & mustajuurisosekeittoa l g Lisäkkeenä: paahtopaistia, salaattijuustoa, tofua'
	],
	details: {
		address: 'Kalevantie 2, 33100 Tampere',
		homepage: 'www.ravintola-aleksis.fi',
		stars: '3.3 (3 arviota)',
		openingHours: 'ma-pe: 10:30-13:30',
		restaurantType: '',
		kitchenType: ''
	}
}
