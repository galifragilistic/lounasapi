import data = require('./modules/data')
import { parseRestaurantName, parseRestaurantMenu } from './modules/menuparser'
import { searchbykeyword } from './modules/search'
import { getMenus } from './modules/menulister'
import { getSlackMessage, getSlackAction } from './modules/slackresponder'
import DbConnection from './models/Connection'
import SlackMessage from './models/slack/SlackMessage'

export async function menus(date: string | null, connection: DbConnection) {
	return await getMenus(date, connection)
}
export async function search(keyword: string, searchUrl: string) {
	return await searchbykeyword(keyword, searchUrl)
}
export async function restaurants(connection: DbConnection) {
	return await data.getRestaurants(connection)
}
export async function addRestaurant(url: string, connection: DbConnection) {
	const name = await parseRestaurantName(url)
	return await data.addRestaurant(url, name, connection)
}
export async function deleteRestaurant(url: string, connection: DbConnection) {
	return await data.deleteRestaurant(url, connection)
}
export async function slackCommand(text: string, connection: DbConnection) {
	return await getSlackMessage(text, connection)
}
export async function slackAction(user: any, message: string, actions: any) {
	return await getSlackAction(user, message, actions)
}
export function slackMessage(text: string, isprivate = false) {
	const msg = new SlackMessage()
	msg.response_type = isprivate ? 'ephemeral' : 'in_channel'
	msg.text = text
	return msg
}
