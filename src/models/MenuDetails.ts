class MenuDetails {
	public address: string = ''
	public homepage: string = ''
	public stars: string = ''
	public openingHours: string = ''
	public restaurantType: string = ''
	public kitchenType: string = ''
}

export default MenuDetails
