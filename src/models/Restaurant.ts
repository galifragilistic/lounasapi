class Restaurant {
	public _id: string = ''
	public url: string = ''
	public name: string = ''

	constructor(url: string, name: string, id?: string) {
		this.url = url
		this.name = name
		this._id = id || ''
	}
}

export default Restaurant
