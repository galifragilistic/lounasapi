import Menu from '../Menu'

class SlackMessage {
	// tslint:disable-next-line: variable-name
	public response_type: string = 'in_channel'
	public text: string = 'Päivän lounakset'
	public blocks: any[] = []

	public addSection(text: string) {
		this.blocks.push(section(text))
	}

	public attachMenuBlocks(menus: Menu[]) {
		let menutext = ''
		menus.forEach(e => {
			menutext += '*' + e.name + '*\n'
			e.menuItems.forEach(menuitem => {
				menutext += menuitem + '\n'
			})
			this.addButtonSection(menutext, 'Vote', e.name)
			this.addVotingContext()
			this.addDivider()
			menutext = ''
		})
	}

	public message(text: string, isprivate = false) {
		const msg = new SlackMessage()
		msg.response_type = isprivate ? 'ephemeral' : 'in_channel'
		msg.text = text
		return msg
	}

	public addButtonSection(text: any, buttonText: string, value: any) {
		const sec = section(text)
		sec.accessory = buttonAccessory(buttonText, value)
		this.blocks.push(sec)
	}

	private addDivider() {
		this.blocks.push(divider)
	}

	private addVotingContext() {
		this.blocks.push(context)
	}
}

const divider = { type: 'divider' }

const context = {
	type: 'context',
	elements: [
		{
			type: 'mrkdwn',
			text: 'Ei ääniä'
		}
	]
}

function section(text: string) {
	const accessory: any = ''
	return {
		type: 'section',
		text: {
			type: 'mrkdwn',
			text
		},
		accessory
	}
}

function buttonAccessory(buttonText: string, value: any) {
	return {
		type: 'button',
		text: {
			type: 'plain_text',
			text: buttonText
		},
		value
	}
}

export default SlackMessage
