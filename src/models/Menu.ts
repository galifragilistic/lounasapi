import MenuDetails from '../models/MenuDetails'

class Menu {
	public url: string = ''
	public name: string = ''
	public details: MenuDetails
	public menuItems: string[] = []

	constructor() {
		this.details = new MenuDetails()
	}
}

export default Menu
