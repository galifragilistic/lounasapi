class DbConnection {
	public connStr = ''
	public dbName = ''
	public collectionName = ''

	constructor(connstr: string, dbname: string, collectionName: string) {
		this.connStr = connstr
		this.dbName = dbname
		this.collectionName = collectionName
	}
}

export default DbConnection
