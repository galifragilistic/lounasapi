import $ = require('cheerio')
import Restaurant from '../models/Restaurant'
import html = require('./html')

export async function searchbykeyword(keyword: string, searchUrl: string) {
	if (!keyword.trim()) {
		return []
	}
	const url = searchUrl + keyword

	const htmlData = await html.getHtml(url).catch(() => {
		return ''
	})

	return parseresults(htmlData)
}

function parseresults(htmlString: string) {
	const divs = $('div.menu', htmlString)
	const restaurants: Restaurant[] = []

	if (!divs.length) {
		return []
	}

	divs.each((i, div) => {
		const itemHeader = $('div.item-header', div)
		const a = $('h3 > a', itemHeader)
		const url = a.attr('href')
		const name = a.text()
		restaurants.push(new Restaurant(url, name))
	})

	return restaurants
}
