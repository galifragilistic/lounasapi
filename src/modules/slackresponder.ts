import parser = require('../modules/menuparser')
import datab = require('../modules/data')
import menulister = require('../modules/menulister')
import DbConnection from '../models/Connection'
import SlackMessage from '../models/slack/SlackMessage'

export async function getSlackMessage(text: string, connection: DbConnection) {
	const commands = text.split(' ')
	switch (commands[0]) {
		case 'lista':
			return getRestaurantList(connection)
		case 'uusi':
			if (commands.length < 2) {
				return new SlackMessage().message(
					'Ohje: /lounas uusi <ravintolan lounas.info-url>',
					true
				)
			}
			const url = commands[1]
			return await addRestaurant(url, connection)
		default:
			return await getMenuSlackMessage
	}
}
export function getSlackAction(user: any, message: string, actions: any) {
	const username = `<@${user.id}>`
	const blockid = actions[0].block_id
	return addVote(message, blockid, username)
}

function addVote(message: any, blockid: string, username: string) {
	const blockindex = message.blocks.findIndex(
		(b: { block_id: string }) => b.block_id === blockid
	)
	if (blockindex === -1) {
		return false
	}
	const context = message.blocks[blockindex + 1]
	const usernameindex = context.elements.findIndex(
		(e: { text: string }) => e.text === username
	)
	if (usernameindex !== -1) {
		// unvote
		context.elements.splice(usernameindex, 1)
		if (context.elements.length === 0) {
			// add "no votes"
			context.elements.push({
				type: 'mrkdwn',
				text: 'Ei ääniä'
			})
		}
	} else {
		if (context.elements.find((e: { text: string }) => e.text === 'Ei ääniä')) {
			context.elements = []
		}

		const vote = {
			type: 'mrkdwn',
			text: username
		}

		context.elements.push(vote)
	}
	message.blocks.splice(blockindex + 1, 1, context)

	return message
}

async function getRestaurantList(connection: DbConnection) {
	const msg = new SlackMessage()
	const restaurants = await datab.getRestaurants(connection)
	restaurants.forEach(r => {
		msg.addButtonSection(r.name, 'Poista', r._id)
	})

	return msg
}

async function addRestaurant(url: string, connection: DbConnection) {
	const msg = new SlackMessage()
	let name = ''
	name = await parser.parseRestaurantName(url)
	let success = true

	await datab.addRestaurant(url, name, connection).catch(() => {
		success = false
	})

	return success
		? msg.message('Ravintolan lisäys onnistui', true)
		: msg.message('Ravintolan lisäys ei onnistunut', true)
}

async function getMenuSlackMessage(connection: DbConnection) {
	const menus = await menulister.getMenus(null, connection)
	const msg = new SlackMessage()

	if (menus.length) {
		msg.attachMenuBlocks(menus)
		return msg
	} else {
		return msg.message('Ei ravintoloita')
	}
}
