import MongoClient = require('mongodb')
import DbConnection from '../models/Connection'
import Restaurant from '../models/Restaurant'

export function getRestaurants(connection: DbConnection) {
	return new Promise<Restaurant[]>((resolve, reject) => {
		MongoClient.connect(connection.connStr, (err, db) => {
			if (err) {
				reject(err)
			}

			const dbo = db.db(connection.dbName)
			dbo
				.collection(connection.collectionName)
				.find()
				.toArray((error, res) => {
					if (error) {
						reject(error)
					}
					db.close()
					resolve(res)
				})
		})
	})
}
export function addRestaurant(
	url: string,
	name: string,
	connection: DbConnection
) {
	return new Promise<MongoClient.InsertOneWriteOpResult>((resolve, reject) => {
		MongoClient.connect(connection.connStr, (err, db) => {
			if (err) {
				reject(err)
			}
			const dbo = db.db(connection.dbName)
			const obj = { url, name }
			dbo.collection(connection.collectionName).insertOne(obj, (error, res) => {
				if (error) {
					reject(error)
				}
				db.close()
				resolve(res)
			})
		})
	})
}
export function deleteRestaurant(url: string, connection: DbConnection) {
	return new Promise<MongoClient.DeleteWriteOpResultObject>(
		(resolve, reject) => {
			MongoClient.connect(connection.connStr, (err, db) => {
				if (err) {
					reject(err)
				}
				const dbo = db.db(connection.dbName)
				const query = { url }
				dbo
					.collection(connection.collectionName)
					.deleteOne(query, (error, res) => {
						if (error) {
							reject(error)
						}
						db.close()
						resolve(res)
					})
			})
		}
	)
}
export function createDatabase(connection: DbConnection) {
	return new Promise<MongoClient.MongoClient>((resolve, reject) => {
		MongoClient.connect(connection.connStr, (err, db) => {
			if (err) {
				reject(err)
			}
			db.close()
			resolve(db)
		})
	})
}
export function createCollection(
	collectionName: string,
	connection: DbConnection
) {
	return new Promise<MongoClient.Collection<any>>((resolve, reject) => {
		MongoClient.connect(connection.collectionName, (err, db) => {
			if (err) {
				reject(err)
			}
			const dbo = db.db(connection.dbName)
			dbo.createCollection(collectionName, (error, res) => {
				if (err) {
					reject(error)
				}
				db.close()
				resolve(res)
			})
		})
	})
}
