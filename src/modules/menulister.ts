import parser = require('./menuparser')
import datab = require('../modules/data')
import moment from 'moment'
import DbConnection from '../models/Connection'
import Menu from '../models/Menu'

export async function getMenus(date: string | null, connection: DbConnection) {
	if (date === null) {
		date = moment().format('YYYYMMDD')
	}
	const restaurants = await datab.getRestaurants(connection)
	const menus: Menu[] = []

	for (const r of restaurants) {
		const menu = await parser.parseRestaurantMenu(r.url, date!)
		menus.push(menu)
	}

	return menus
}
