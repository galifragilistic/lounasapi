import https = require('https')

export function getHtml(url: string) {
	return new Promise<string>((resolve, reject) => {
		try {
			https
				.get(url, response => {
					let data = ''
					response.on('data', chunk => {
						data += chunk
					})
					response.on('end', () => {
						resolve(data)
					})
				})
				.on('error', err => {
					reject(err)
				})
		} catch (ex) {
			reject(ex)
		}
	})
}
