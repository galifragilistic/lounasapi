import moment = require('moment')
import $ = require('cheerio')
import MenuDetails from '../models/MenuDetails'
import Menu from '../models/Menu'
import html = require('./html')

export async function parseRestaurantName(url: string) {
	const htmlData = await html.getHtml(url).catch(() => {
		return ''
	})
	return parsename(htmlData)
}

export async function parseRestaurantMenu(url: string, date: string) {
	const htmlData = await html.getHtml(url).catch(() => {
		return ''
	})

	const menu = new Menu()
	const curDate = moment(date, 'YYYYMMDD')
	const curDay = curDate.day()
	if (curDay === 0 || curDay === 6) {
		const substract = curDay === 0 ? -2 : -1
		curDate.add(substract, 'd')
	}
	const details = parsedetails(htmlData)
	let menuitems: string[] = []
	if (details !== null) {
		menuitems = parsemenuitems(htmlData, curDate.format('D.M'))
	}
	menu.details = details
	menu.menuItems = menuitems

	return menu
}

function parsename(htmlString: string) {
	const nameElem = $(
		'div.tile-container > div.tile-full > h2[itemprop="name"]',
		htmlString
	)
	const name = nameElem.text().trim()
	return name
}

function parsedetails(htmlString: string) {
	const details = new MenuDetails()

	const headerTile = $('div.tile-container', htmlString)
	if (!headerTile.length) {
		return details
	}

	const streetAddress = $('span[itemprop="streetAddress"]', headerTile).text()
	const postalCode = $('span[itemprop="postalCode"]', headerTile).text()
	const city = $('span[itemprop="addressLocality"]', headerTile).text()
	details.address = `${streetAddress}, ${postalCode} ${city}`

	details.homepage = $('a[itemprop="url"]', headerTile).text()

	const pOpeningHours = $('p:has(meta[itemprop="openingHours"])', headerTile)
	details.openingHours = pOpeningHours.text().trim()

	const pStars = $('p:has(i[class="fa fa-star"])', headerTile)
	details.stars = pStars.text().trim()

	details.restaurantType = $('span.restaurant-type', headerTile)
		.text()
		.trim()
	details.kitchenType = $('span.kitchen-type', headerTile)
		.text()
		.trim()

	return details
}

function parsemenuitems(htmlString: string, curDate: string) {
	const divs = $('div.item:has(div.item-header)', htmlString)
	let curItem: CheerioElement
	const menuitems: string[] = []

	for (let i = 0; i < 5; i++) {
		curItem = divs[i]
		const datedata = $('div.item-header > h3', curItem)
		const split = datedata.text().split(' ')
		let date = split[1]
		date = date.substring(0, date.length - 1)

		if (date.toString() !== curDate.toString()) {
			continue
		}

		const menuitemlist = $('div.item-body > ul > li.menu-item', curItem)
		menuitemlist.each((_i, e) => {
			menuitems.push(
				$('p.dish', e)
					.text()
					.trim()
					.replace(/\s+/g, ' ')
			)
			if ($('p.info', e).text().length > 0) {
				menuitems.push(
					$('p.info', e)
						.text()
						.trim()
						.replace(/\s+/g, ' ')
				)
			}
		})
	}
	return menuitems
}
