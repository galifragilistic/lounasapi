import search = require('../modules/search')
import fs = require('fs')
import path = require('path')
import html = require('../modules/html')
import Restaurant from '../models/Restaurant'

const htmlDataOneResult = fs
	.readFileSync(
		path.resolve(__dirname, '../__fixtures__/searchResponseOneResult.html'),
		'utf8'
	)
	.toString()

const htmlDataNoResults = fs
	.readFileSync(
		path.resolve(__dirname, '../__fixtures__/searchResponseNoResults.html'),
		'utf8'
	)
	.toString()

describe('search', () => {
	const mockGetHtml = jest.spyOn(html, 'getHtml')

	it('should get one result', () => {
		const expectedOneResult = [
			new Restaurant('/lounas/aleksis/tampere', 'Aleksis')
		]
		mockGetHtml.mockImplementation(() => Promise.resolve(htmlDataOneResult))
		return search.searchbykeyword('keyword', 'url').then(data => {
			expect(html.getHtml).toHaveBeenCalled()
			expect(data).toMatchObject(expectedOneResult)
		})
	})
	it('should get no results', () => {
		mockGetHtml.mockImplementation(() => Promise.resolve(htmlDataNoResults))
		return search.searchbykeyword('keyword', 'url').then(data => {
			expect(html.getHtml).toHaveBeenCalled()
			expect(data).toMatchObject([])
		})
	})
})
