import fs = require('fs')
import path = require('path')
import html = require('../modules/html')
import { parseRestaurantName, parseRestaurantMenu } from '../modules/menuparser'
import { monday, thursday, friday } from '../__fixtures__/aleksisMenu'

const restaurantHtml = fs
	.readFileSync(
		path.resolve(__dirname, '../__fixtures__/restaurantPage.html'),
		'utf8'
	)
	.toString()

const mockGetHtml = jest.spyOn(html, 'getHtml')
mockGetHtml.mockImplementation(() => Promise.resolve(restaurantHtml))

describe('name parser', () => {
	it('should parse name', () => {
		return parseRestaurantName('url').then(name => {
			expect(html.getHtml).toHaveBeenCalled()
			expect(name).toEqual('Aleksis')
		})
	})
})
describe('menu parser', () => {
	it('should return monday menu', () => {
		return parseRestaurantMenu('ur', '20190916').then(menu => {
			expect(html.getHtml).toHaveBeenCalled()
			expect(menu).toMatchObject(monday)
		})
	})
	it('should return thursday menu', () => {
		return parseRestaurantMenu('ur', '20190919').then(menu => {
			expect(menu).toMatchObject(thursday)
		})
	})
	it('should return friday menu on saturday', () => {
		return parseRestaurantMenu('ur', '20190921').then(menu => {
			expect(menu).toMatchObject(friday)
		})
	})
	it('should return friday menu on sunday', () => {
		return parseRestaurantMenu('ur', '20190922').then(menu => {
			expect(menu).toMatchObject(friday)
		})
	})
})
